﻿#include <iostream>
#include <string>

int main()
{
    //Ввод данных
    std::cout << "Enter your string: ";
    std::string name;
    std::getline (std::cin, name);

    //Вывод данных
    std::cout << "Your string: " << name << '\n';
    std::cout << "String length: " << name.length() << '\n';
    std::cout << "First simbol: " << name[0] << '\n';
    std::cout << "Last simbol: " << name[name.length()-1] << '\n';

}